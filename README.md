# Base Angular Project

## Generar Proyecto con estilos SASS
`ng new base-project --style=scss`

## Excluir los componentes `*.component.spec.ts`
1. Eliminar app.component.spec.ts
2. Configuración para excluir `*.spec.ts` en `ng g c/s`
    Editar `'defaults'` de `.angular-cli.json`
    ```
    "defaults": {
        "styleExt": "scss",
        "component": {
            "spec": false
        },
        "service": {
            "spec": false
        }
    }
    ```

## Esctructura de proyecto
1. Generar componente y modulo para partes publicas del proycto como login, etc.  
    1.1. `ng g c public`  
    1.2. `ng g m public`  
2. Generar componente y modulo para partes privadas del proyecto como perfil, etc.  
    2.1. `ng g c private`  
    2.2. `ng g m private`  

    NOTA: quitar la importacion de los componentes anteriores de `app.module.ts` ya que los modulos lo exportan por medio de `Lazy Loading`
3. Crear carpeta `shared` a nivel de `src` para archivos que seran utilizados por otros componentes
    ```
    /shared
        /components *componentes reutilizables*
        /guards *validadores de rutas*
        /interfaces *estructura de datos*
        /pipes *edicones de vistas, etc...*
        /services *servicios para obtencion o envio de informacion*
        /etc...
    ```
4. En la carpeta `assets` crear directorios para separar los recursos  
    ```
    /assets
        /scss
        /img
        /fixtures
    ```

## Servicios
1. las rutas de conexion a APIs deben estar en `enviroments/enviroment.ts`
2. dentro de la carpeta `/shared/services` crear `api.service.ts`  
    Este será el servicio que realizará todas las peticiones
3. crear servicio correspondiente al componente o un servicio para una tarea determinada si esto es necesario
4. Para realizar peticiones HTTP utilizar `HttpClient` integrado en Angular 4.3
5. Promise vs Observable  
    5.1 Una promesa realiza la peticion y devuelve la respuesta del servicio.  
    5.2 Un obsevable además de ralizar la peticion puede quedarse escuchando cambios. 

    NOTA: Un observable es recomendado para algún chat, notificaciones, etc. para las peticiones normales, utilizar promesas. En el caso de usar observables, desuscribirse al final.  
    https://stackoverflow.com/questions/37364973/angular-promise-vs-observable  
    http://brianflove.com/2016/12/11/anguar-2-unsubscribe-observables/
