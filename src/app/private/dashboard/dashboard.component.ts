import { Component, OnInit, OnDestroy } from '@angular/core';

import { DashboardService } from '../../../shared/services/dashboard.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {
  posts = [];
  subscribe: Subscription;

  constructor(private dashboardService: DashboardService) { }

  ngOnInit() {
    this.getDashboard();
  }

  ngOnDestroy() {
    this.subscribe.unsubscribe();
  }

  getDashboard() {
    /* this.dashboardService.getDashboard()
      .then((success: any[]) => {
        this.posts = success;
      }); */
    this.subscribe = this.dashboardService.getDashboardObs()
      .subscribe((success: any[]) => {
        this.posts = success;
      });
  }

}
