import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DashboardService } from '../../shared/services/dashboard.service';

const PRIVATE_ROUTES: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(PRIVATE_ROUTES)
  ],
  declarations: [DashboardComponent],
  providers: [
    DashboardService
  ]
})
export class PrivateModule { }
