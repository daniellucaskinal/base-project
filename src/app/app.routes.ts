import { RouterModule, Routes } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';


const ROUTES: Routes = [
    { path: '', redirectTo: 'public', pathMatch: 'full' },
    { path: 'public', loadChildren: './public/public.module#PublicModule'},
    { path: 'private', loadChildren: './private/private.module#PrivateModule'}
];

export const APP_ROUTING: ModuleWithProviders = RouterModule.forRoot(ROUTES);
