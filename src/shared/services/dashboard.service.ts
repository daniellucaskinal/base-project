import { Injectable } from '@angular/core';

import { ApiService } from './api.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class DashboardService {

  path = 'posts';

  constructor(private apiService: ApiService) { }

  getDashboard(): Promise<any> {
    // Path final: 'https://jsonplaceholder.typicode.com/posts'
    return this.apiService.getPromise(this.path);
  }

  getDashboardObs(): Observable<any> {
    return this.apiService.getSubsribe(this.path);
  }

}
