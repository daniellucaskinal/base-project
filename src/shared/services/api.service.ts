import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { url } from '../../environments/environment';

@Injectable()
export class ApiService {

  apiUrl = url;

  constructor(private httpClient: HttpClient) { }

  getPromise(path: string): Promise<any> {
    return this.httpClient.get(`${this.apiUrl}/${path}`)
      .toPromise()
      .catch(this.handleError);
  }

  getSubsribe(path: string): Observable<any> {
    return this.httpClient.get(`${this.apiUrl}/${path}`);
  }

  private handleError(error: any) {
    console.error('Error', error.message || error);
  }

}
